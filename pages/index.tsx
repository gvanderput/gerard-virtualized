import List from "../components/List/List";
import Preview from "../components/Preview/Preview";

export default function Home() {
  return (
    <>
      <List />
      <Preview />
    </>
  );
}

export async function getStaticProps() {
  return {
    props: {},
  };
}
