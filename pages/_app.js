import "semantic-ui-css/semantic.min.css";
import "../styles/globals.css";
import MainMenu from "../components/MainMenu";
import { Container, Segment } from "semantic-ui-react";
import ListContextProvider from "../context/ListContextProvider";

function MyApp({ Component, pageProps }) {
  return (
    <ListContextProvider>
      <Container className="mainContainer">
        <MainMenu />
        <Segment>
          <Component {...pageProps} />
        </Segment>
      </Container>
    </ListContextProvider>
  );
}

export default MyApp;
