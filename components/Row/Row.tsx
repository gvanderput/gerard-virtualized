import styles from "./row.module.scss";
import classnames from "classnames";

const Row: React.FC<RowProps> = ({ item, ...props }) => {
  return (
    <div
      {...props}
      data-index={item.index}
      className={classnames(styles.row, {
        [styles.secondary]: item.index % 2 === 0,
      })}
    >
      <div className={styles.title}>Row</div>
      <div className={styles.subtitle}>{item.text}</div>
    </div>
  );
};

export default Row;
