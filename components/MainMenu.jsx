import { Menu, Container } from "semantic-ui-react";
import { useRouter } from "next/router";
import Link from "next/link";

export default function MainMenu() {
  const { pathname } = useRouter();

  const buttons = [{ path: "/", text: "List" }];

  return (
    <Menu
      fixed="top"
      inverted
      color="blue"
      style={{ border: "5px solid white" }}
    >
      <Container fluid>
        <Menu.Item header>Virtualization</Menu.Item>
        {buttons.map((button) => (
          <Link key={button.text} href={button.path}>
            <Menu.Item active={pathname === button.path}>
              {button.text}
            </Menu.Item>
          </Link>
        ))}
      </Container>
    </Menu>
  );
}
