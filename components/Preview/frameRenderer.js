function frameRenderer(size, contextValues) {
  this.clearRect(0, 0, size.width, size.height);

  const {
    viewportHeight,
    amountRows,
    rowHeight,
    scrollTop,
    indexStart,
    indexEnd,
  } = contextValues;

  const contentHeight = amountRows * rowHeight;
  const scale = viewportHeight / contentHeight;

  // bg
  this.save();
  this.beginPath();
  this.rect(0, 0, size.width, size.height);
  this.fillStyle = "rgba(0, 0, 0, 0.1)";
  this.strokeStyle = "black";
  this.fill();
  this.stroke();
  this.restore();

  // data
  for (let i = indexStart; i <= indexEnd; i++) {
    this.save();
    this.translate(0.5, 0.5);
    this.beginPath();
    this.rect(
      0,
      i * rowHeight * scale + 0.5,
      size.width - 1,
      rowHeight * scale
    );
    this.fillStyle = `rgba(0, 255, 0, ${i % 2 === 0 ? "0.2" : "0.4"})`;
    this.fill();
    this.restore();
  }

  // viewport
  this.save();
  //this.translate(0.5, 0.5);
  this.beginPath();
  this.rect(
    1,
    scrollTop * scale + 1,
    size.width - 2,
    viewportHeight * scale - 2
  );
  this.lineWidth = 2;
  this.strokeStyle = "black";
  this.stroke();
  this.restore();
}

export default frameRenderer;
