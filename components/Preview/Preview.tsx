import { useCallback, useContext, useEffect, useRef } from "react";
import styles from "./preview.module.scss";
import frameRenderer from "./frameRenderer";
import ListContext from "../../context/ListContext";

export default function Preview() {
  const contextValues = useContext(ListContext);

  const canvasRef = useRef(null);
  const requestIdRef = useRef(null);
  const valuesRef = useRef<any>({});
  const size = { width: 100, height: 400 };

  const renderFrame = () => {
    const ctx = canvasRef.current.getContext("2d");
    frameRenderer.call(ctx, size, valuesRef.current);
  };

  useEffect(() => {
    valuesRef.current = contextValues;
  }, [contextValues]);

  const tick = () => {
    if (!canvasRef.current) return;
    renderFrame();
    requestIdRef.current = requestAnimationFrame(tick);
  };

  useEffect(() => {
    requestIdRef.current = requestAnimationFrame(tick);
    return () => {
      cancelAnimationFrame(requestIdRef.current);
    };
  }, []);

  return (
    <div className={styles.preview}>
      <canvas {...size} ref={canvasRef} />
    </div>
  );
}
