import React from "react";
import { ContextValues } from "./ListContextProvider";

const ListContext = React.createContext<ContextValues>(null);
export default ListContext;
