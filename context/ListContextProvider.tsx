import { useRef, useState } from "react";
import ListContext from "./ListContext";
import { Dispatch, MutableRefObject, SetStateAction } from "react";

export type ContextValues = {
  scrollTop: number;
  setScrollTop: Dispatch<SetStateAction<number>>;
  rowsRef: MutableRefObject<HTMLDivElement>;
  viewportHeight: number;
  amountRows: number;
  rowHeight: number;
  indexStart: number;
  indexEnd: number;
  oncePerMs: number;
  amountRowsBuffered: number;
};

export default function ListContextProvider({ children }) {
  const [scrollTop, setScrollTop] = useState<number>(0);
  const rowsRef = useRef<HTMLDivElement>(null);
  const viewportHeight = 400;
  const amountRows = 40;
  const rowHeight = 40;
  const amountRowsBuffered = 2;
  const indexStart = Math.max(
    Math.floor(scrollTop / rowHeight) - amountRowsBuffered,
    0
  );
  const indexEnd = Math.min(
    Math.ceil((scrollTop + viewportHeight) / rowHeight - 1) +
      amountRowsBuffered,
    amountRows - 1
  );
  const oncePerMs = 20;

  const value: ContextValues = {
    scrollTop,
    setScrollTop,
    rowsRef,
    viewportHeight,
    amountRows,
    rowHeight,
    indexStart,
    indexEnd,
    oncePerMs,
    amountRowsBuffered,
  };

  return <ListContext.Provider value={value}>{children}</ListContext.Provider>;
}
