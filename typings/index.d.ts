type DataItem = {
  index: number;
  text: string;
  top: number;
};

interface RowProps extends React.HTMLAttributes<HTMLDivElement> {
  item: DataItem;
}
